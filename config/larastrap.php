<?php

return [
    /*
        All the values in "commons" and the different arrays into "elements" may be defined everywhere.
        Those specific for an "elements" tag type will take precedence on those defined in "commons".
    */

    'commons' => [
        'label_width' => '4',
        'input_width' => '8',
    ],

    'elements' => [
        'navbar' => [
            'color' => 'light',
        ],

        'modal' => [
            'buttons' => [
                ['color' => 'secondary', 'label' => 'Close', 'attributes' => ['data-bs-dismiss' => 'modal']]
            ],
        ],

        'form' => [
            'formview' => 'horizontal',
            'method' => 'POST',

            'buttons' => [
                ['color' => 'primary', 'label' => 'Save', 'attributes' => ['type' => 'submit']]
            ]
        ],

        'number' => [
            'step' => 1,
            'min' => PHP_INT_MIN,
            'max' => PHP_INT_MAX,
        ],

        'range' => [
            'step' => 1,
            'min' => PHP_INT_MIN,
            'max' => PHP_INT_MAX,
        ],

        'radios' => [
            'color' => 'outline-primary',
        ],

        'checks' => [
            'color' => 'outline-primary',
        ],

        'tabs' => [
            'tabview' => 'tabs',
        ],

        'tabpane' => [
            'classes' => ['p-3', 'card']
        ]
    ],

    'customs' => [
        'command' => [
            'extends' => 'text',
            'params' => [
                'classes' => ['command'],
                'attributes' => ['readonly' => 'readonly'],
                'textappend' => '<i class="bi bi-clipboard"></i>',
                'squeeze' => true,
            ],
        ],

        'title' => [
            'extends' => 'link',
            'params' => [
                'override_classes' => ['title-link'],
                'reviewCallback' => function($node, $params) {
                    $identifier = Str::slug($params['label']);
                    $params['id'] = $identifier;
                    $params['label'] = sprintf('<h2>%s</h2>', $params['label']);
                    $params['href'] = sprintf('#%s', $identifier);
                    return $params;
                }
            ],
        ],

        'mybutton' => [
            'extends' => 'button',
            'params' => [
                'color' => 'warning',
            ]
        ],

        'element' => [
            'extends' => 't',
            'params' => [
                'classes' => ['text-danger'],
                'node' => 'strong',
            ]
        ],

        'value' => [
            'extends' => 't',
            'params' => [
                'node' => 'code',
            ]
        ],

        'code' => [
            'extends' => 't',
            'params' => [
                'node' => 'code',
            ]
        ],

        'parameter' => [
            'extends' => 't',
            'params' => [
                'node' => 'strong',
            ]
        ]
    ],
];
