<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CommonController extends Controller
{
    public function homepage()
    {
        return view('welcome');
    }

    public function customIndex()
    {
        app()->make('LarastrapStack')->addCustomElement('jerkbutton', [
            'extends' => 'button',
            'params' => [
                'color' => 'secondary',
                'postlabel' => ' (Click Here!)',
            ],
        ]);

        return view('docs.custom-elements');
    }

    public function customExamples()
    {
        $stack = app()->make('LarastrapStack');
        $path = base_path('resources/views/samples/recipes/');
        $recipes = scandir($path);
        $data = [];

        foreach($recipes as $foldername) {
            if ($foldername == '.' || $foldername == '..') {
                continue;
            }

            $folder = $path . $foldername;
            if (is_dir($folder)) {
                $config = $folder . '/config.php';
                if (file_exists($config)) {
                    $config_contents = file_get_contents($config);
                    eval('$config = ' . $config_contents . ';');

                    foreach($config['customs'] as $name => $custom_config)
                    $stack->addCustomElement($name, $custom_config);

                    $data[] = (object) [
                        'folder' => $foldername,
                        'description' => file_get_contents($folder . '/description.txt'),
                        'config' => $config_contents,
                    ];
                }
            }
        }

        return view('docs.custom-examples', ['recipes' => $data]);
    }

    public function fakePost()
    {
        return redirect()->route('homepage');
    }

    public function validatedRoute(Request $request)
    {
        $validated = $request->validateWithBag('special', [
            'email' => 'required|email|max:255',
            'name' => 'required',
        ]);

        return redirect()->route('homepage');
    }
}
