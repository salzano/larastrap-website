# Larastrap Website

Published at https://larastrap.madbob.org/

To submit a new Larastrap Recipe, [get a look here](https://gitlab.com/madbob/larastrap-website/-/tree/master/resources/views/recipes).
