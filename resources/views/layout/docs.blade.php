@extends('layout.app', [
    'title' => $title,
    'claim' => $claim,
])

@section('contents')

@php

$menu = [
    'Intro' => [
        'Getting Started' => route('docs.getting-started'),
    ],
    'Components' => [
        'Base Element' => route('docs.element'),
        'Typography' => route('docs.text'),
        'Navbar' => route('docs.navbar'),
        'Buttons' => route('docs.button'),
    ],
    'Forms' => [
        'Overview' => route('docs.forms'),
        'Fields' => route('docs.field'),
        'Inputs' => route('docs.input'),
        'Selects' => route('docs.select'),
        'Checks and Radios' => route('docs.check-radio'),
    ],
    'Containers' => [
        'Overview' => route('docs.containers'),
        'Button Groups' => route('docs.button-group'),
        'Modal' => route('docs.modal'),
        'Accordion' => route('docs.accordion'),
        'Collapse' => route('docs.collapse'),
        'Tabs' => route('docs.tabs'),
        'Enclose' => route('docs.enclose'),
    ],
    'Custom Elements' => [
        'Overview' => route('docs.custom-elements'),
        'Examples' => route('docs.custom-examples'),
    ],
    'More' => [
        'Translations' => route('docs.translations'),
    ]
];

@endphp

<div class="container mt-3">
    <div class="row mb-5">
        <div class="d-none d-lg-block col-lg-2 d-lg-block">
            <ul id="docs-menu" class="list-group sticky-top">
                @foreach($menu as $header => $children)
                    <li class="list-group-item">
                        <strong>{{ $header }}</strong>
                        <ul class="list-group">
                            @foreach($children as $label => $route)
                                <a href="{{ $route }}" class="list-group-item list-group-item-action">{{ $label }}</a>
                            @endforeach
                        </ul>
                    </li>
                @endforeach
            </ul>
        </div>

        <div class="col-12 col-lg-10 ps-5">
            <div class="dropdown-center mb-5 d-block d-lg-none">
                <button class="btn btn-primary dropdown-toggle w-100" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                    Explore the Docs
                </button>
                <ul class="dropdown-menu vw-100 bg-dark">
                    @foreach($menu as $header => $children)
                        <li><h6 class="dropdown-header">{{ $header }}</h6></li>
                        @foreach($children as $label => $route)
                            <li><a href="{{ $route }}" class="dropdown-item text-white">{{ $label }}</a></li>
                        @endforeach
                    @endforeach
                </ul>
            </div>

            @yield('docs')
        </div>
    </div>
</div>

@endsection
