<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>{{ $title }}</title>
        <meta name="description" content="{{ $claim }}">

        <link rel="stylesheet" href="{{ mix('css/app.css') }}">

        <meta name="twitter:card" content="summary_large_image">
        <meta name="twitter:title" content="{{ $title }}">
        <meta name="twitter:description" content="{{ $claim }}">
        <meta name="twitter:creator" content="@madbob">
        <meta name="twitter:image" content="https://larastrap.madbob.org/images/fb.png">

        <meta property="og:site_name" content="Larastrap" />
        <meta property="og:title" content="Larastrap" />
        <meta property="og:url" content="https://larastrap.madbob.org/" />
        <meta property="og:image" content="https://larastrap.madbob.org/images/fb.png" />
        <meta property="og:type" content="website" />
        <meta property="og:locale" content="en" />

        <script type="application/ld+json">
        {
            "@context" : "http://schema.org",
            "@type" : "WebApplication",
            "name" : "Larastrap",
            "abstract": "Opinionated Bootstrap 5 components for Laravel",
            "author" : {
                "@type" : "Person",
                "name" : "Roberto Guido",
                "sameAs": "https://www.madbob.org/"
            },
            "offers": {
                "@type" : "Offer",
                "price": 0,
                "priceCurrency": "EUR",
                "name": "Free and Open Source"
            },
            "applicationCategory" : ["DeveloperApplication", "https://www.wikidata.org/wiki/Q1330336"],
            "operatingSystem" : "web",
            "url" : "https://larastrap.madbob.org/",
            "keywords": "laravel, bootstrap, components",
            "license": "https://spdx.org/licenses/MIT.html",
            "about": [
                {
                    "@type" : "Thing",
                    "name": "Laravel",
                    "sameAs": "https://www.wikidata.org/wiki/Q13634357"
                },
                {
                    "@type" : "Thing",
                    "name": "Bootstrap",
                    "sameAs": "https://www.wikidata.org/wiki/Q893195"
                }
            ],
            "review": {
                "@type" : "Review",
                "reviewRating": {
                    "@type": "Rating",
                    "ratingValue": "5"
                },
                "author": {
                    "@type" : "Person",
                    "name" : "Roberto Guido"
                }
            }
        }
        </script>
    </head>
    <body>
        <x-larastrap::navbar title="Larastrap" :title_link="route('homepage')" :options="[
            'Home' => ['route' => 'homepage'],
            'Docs' => ['route' => 'docs.getting-started'],
        ]" />

        @yield('contents')

        <section class="contacts-wrapper">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <h3>Larastrap</h3>
                    </div>
                </div>
                <div class="row">
                    <div class="col links">
                        <div class="row">
                            <div class="col-1">
                                <p><i class="bi-envelope" aria-hidden="true"></i></p>
                            </div>
                            <div class="col-11">
                                <p><a href="mailto:info@madbob.org">info@madbob.org</a></p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-1">
                                <p><i class="bi-code-slash" aria-hidden="true"></i></p>
                            </div>
                            <div class="col-11">
                                <p><a href="https://gitlab.com/madbob/larastrap">gitlab.com/madbob/larastrap</a></p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-1">
                                <p><i class="bi-gear" aria-hidden="true"></i></p>
                            </div>
                            <div class="col-11">
                                <p>powered by <a href="http://madbob.org/"><img src="{{ url('/images/mad.png') }}" alt="MAD"></a></p>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <a href="https://www.paypal.com/donate?hosted_button_id=GRKVVL3E5KZ4Q">
                            <img src="/images/paypal.png" alt="PayPal Donation" border="0">
                        </a>
                    </div>
                </div>
            </div>
        </section>

        <!-- Matomo -->
        <script type="text/javascript">
            var _paq = window._paq || [];
            /* tracker methods like "setCustomDimension" should be called before "trackPageView" */
            _paq.push(["setDoNotTrack", true]);
            _paq.push(["disableCookies"]);
            _paq.push(['trackPageView']);
            _paq.push(['enableLinkTracking']);
            (function() {
            var u="//stats.madbob.org/";
            _paq.push(['setTrackerUrl', u+'matomo.php']);
            _paq.push(['setSiteId', '19']);
            var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
            g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'matomo.js'; s.parentNode.insertBefore(g,s);
            })();
        </script>
        <noscript><p><img src="//stats.madbob.org/matomo.php?idsite=19&amp;rec=1" style="border:0;" alt="" /></p></noscript>
        <!-- End Matomo Code -->

        <script src="{{ mix('js/app.js') }}"></script>
    </body>
</html>
