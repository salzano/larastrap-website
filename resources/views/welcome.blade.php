@extends('layout.app', [
    'title' => 'Larastrap: opinionated Bootstrap5 components for Laravel',
    'claim' => 'Native Blade components for less code and more reusability',
])

@section('contents')
<section class="header-wrapper text-center">
    <div>
        <h1 class="display-1 fw-bold">Larastrap</h1>
        <h2 class="display-5 fw-bold">Opinionated Bootstrap 5 components for Laravel</h2>
    </div>
</section>

<section class="features-wrapper">
    <div class="container">
        <div class="row mt-3 mb-5 justify-content-center">
            <div class="col-12 col-md-4">
                <x-larastrap::command value="composer install madbob/larastrap" />
            </div>
            <div class="col-12 col-md-4">
                <a href="{{ route('docs.getting-started') }}" class="btn btn-lg w-100 h-100 btn-warning d-flex align-items-center justify-content-center">Read The Docs</a>
            </div>
        </div>

        <div class="row mb-5">
            <div class="col lead">
                <p>
                    There are lots of <a href="https://laravel.com/" rel="nofollow">Laravel</a> packages pretending to wrap the widely popular <a href="https://getbootstrap.com/" rel="nofollow">Bootstrap</a> CSS design system.
                    Most of them are limited just to forms formatting (while Bootstrap provides so many different tools), and all of them have an elaborated syntax, so the code required to format your component is no so less verbose (nor more readable) then the actual resulting HTML.
                </p>
                <p>
                    <strong>Larastrap</strong> provides a conspicuous amount of (configurable) defaults and many out-of-the-box common behaviours.
                    And, leveraging the Laravel's native <a href="https://laravel.com/docs/blade#components" rel="nofollow">Blade Components</a> system, provides a few handy automatisms and the ability to define your own reusable Custom Elements.
                </p>
                <p>
                    To reduce to the bare minimum the code you have to actually write (and read, and maintain...) to obtain your desidered layout.
                </p>
            </div>
        </div>

        <div class="row mb-5 align-items-center">
            <div class="col-12 col-md-6">
                <p class="display-5 fw-bold">Get Rid of Boilerplate</p>
                <p>
                    We all love Bootstrap <small>(booo, Tailwind, booo!)</small>, but we all know it also requires a lot of boilerplate code.
                </p>
                <p>
                    Repeated again and again across all the templates.
                </p>
                <p>
                    Here comes Larastrap, which already contains all the boilerplate parts and gives to you just the fun!
                </p>
            </div>
            <div class="col-12 col-md-6">
                @include('partials.example', ['snippet' => 'welcome.base'])
            </div>
        </div>
    </div>

    <div class="purple-bg p-5 mb-5">
        <?php

        $stack = app()->make('LarastrapStack');

        $configs = [
            'Default Configs' => file_get_contents(resource_path('examples/1.php')),
            'Different Layout' => file_get_contents(resource_path('examples/2.php')),
            'Default Colors' => file_get_contents(resource_path('examples/3.php')),
            'Different... All!' => file_get_contents(resource_path('examples/4.php')),
        ];

        ?>

        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="row mb-4 justify-content-center">
                        <div class="col-12 col-md-6 text-white">
                            <p class="display-5 fw-bold">Global Configuration</p>
                            <p>
                                Each component has many parameters that can be set in the global configuration file. Change once any value to an immediate impact on layout, behavior, colors and strings across your application!
                            </p>
                        </div>
                    </div>

                    <x-larastrap::tabs tabview="pills" active="0" classes="nav-fill">
                        @foreach($configs as $label => $conf)
                            <?php $stack->overrideNodesConfig(eval('return ' . $conf . ';')) ?>

                            <x-larastrap::tabpane :label="$label">
                                <div class="row">
                                    <div class="col-12 col-md-5">
                                        <pre class="h-100"><code class="h-100">{{ $conf }}</code></pre>
                                    </div>
                                    <div class="col-12 col-md-7">
                                        <x-larastrap::form>
                                            <x-larastrap::text name="name" label="Name" />
                                            <x-larastrap::text name="surname" label="Surname" />
                                            <x-larastrap::email name="email" label="EMail" />
                                            <x-larastrap::checklist name="email" label="Choose One" :options="['red' => 'Red', 'green' => 'Green', 'blue' => 'Blue']" />
                                        </x-larastrap::form>
                                    </div>
                                </div>
                            </x-larastrap::tabpane>
                        @endforeach
                    </x-larastrap::tabs>

                    <?php $stack->overrideNodesConfig() ?>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row align-items-center">
            <div class="col-12 col-md-6">
                <p class="display-5 fw-bold">Implement Once</p>
                <p>
                    Did you noticed the "copy to clipboard" element on top of this page? It is a <a href="{{ route('docs.custom-elements') }}">Custom Element</a>!
                </p>
                <p>
                    With a few lines of code in the global configuration you can define custom, reusable components to be used in your templates with just a single Blade tag, and to be maintained in a single place with no effort.
                </p>
            </div>
            <div class="col-12 col-md-6">
                @include('partials.custom', ['snippet' => 'customs.command'])
            </div>
        </div>
    </div>
</section>
@endsection
