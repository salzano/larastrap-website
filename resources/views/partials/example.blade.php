<div class="card example card-body">
    <div class="row">
        <div class="col">
            <div class="bg-white p-3">
                <?php Debugbar::startMeasure('render', 'Time for rendering samples.' . $snippet) ?>
                @include('samples.' . $snippet)
                <?php Debugbar::stopMeasure('render'); ?>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col">
            @if(isset($config))
                <pre><code>{!! htmlentities($config) !!}</code></pre>
            @endif
            <pre><code class="language-php">{!! htmlentities(file_get_contents(base_path('resources/views/samples/' . str_replace('.', '/', $snippet) . '.blade.php'))) !!}</code></pre>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <button class="btn btn-info btn-sm ms-2 mb-2" type="button" data-bs-toggle="collapse" data-bs-target="#html_{{ Str::slug($snippet) }}" aria-expanded="false" aria-controls="html_{{ Str::slug($snippet) }}">
                Display generated HTML
            </button>
            <div class="collapse" id="html_{{ Str::slug($snippet) }}">
                <div class="card card-body">
                    <pre><code class="language-html"><?php

                    Debugbar::startMeasure('html', 'Time for rendering HTML for samples.' . $snippet);

                    $html = view('samples.' . $snippet)->render();
                    $config = array(
                        'show-body-only' => true,
                        'indent' => true,
                        'output-xhtml' => false,
                        'wrap' => 2000
                    );

                    $tidy = new \tidy;
                    $tidy->parseString($html, $config, 'utf8');
                    $tidy->cleanRepair();
                    echo htmlentities($tidy);

                    Debugbar::stopMeasure('html');

                    ?></code></pre>
                </div>
            </div>
        </div>
    </div>
</div>
