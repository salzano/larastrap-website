@extends('layout.docs', [
    'title' => 'Larastrap: Base Element',
    'claim' => 'Documentation about the base Elements in Larastrap',
])

@section('docs')

<h1>Base Element</h1>

<p class="lead">
    All components implemented in Larastrap share the same basic class, and the same essential parameters.
</p>

<x-larastrap::title label="id" />

<p>
    The ID of the HTML node. If not specified, a (semi) random one is generated so that all items are properly marked.
</p>

@include('partials.example', ['snippet' => 'base.id'])

<x-larastrap::title label="attributes" />

<p>
    An associative array which is translated to an extra set of attributes to the node. Useful to attach your own data attributes, custom HTML attributes, hooks for your preferred Javascript framework and so on.
</p>

@include('partials.example', ['snippet' => 'base.attributes'])

<x-larastrap::title label="classes" />

<p>
    An array of CSS classes to be added to the HTML node, in addiction to those automatically generated. Can be defined as an array or a space separated string.
</p>

@include('partials.example', ['snippet' => 'base.classes'])

<x-larastrap::title label="override_classes" />

<p>
    An array of CSS classes to be applied to the HTML node, overriding those automatically generated. Can be defined as an array or a space separated string.
</p>

@include('partials.example', ['snippet' => 'base.override_classes'])

<x-larastrap::title label="hidden" />

<p>
    A boolean attribute which hides the element.
</p>

@include('partials.example', ['snippet' => 'base.hidden'])

<x-larastrap::title label="reviewCallback" />

<p>
    A function to be called to directly handle and transform the parameters of the node. This have to take two parameters: the first is the instance of the component itself, the second is the indexed array of parameters. Must return the (eventually modified) array of parameters.
</p>
<p>
    This feature is mostly intended for <a href="{{ route('docs.custom-elements') }}">Custom Elements</a>, and is the ultimate customization tool provided by Larastrap.
</p>

@include('partials.example', ['snippet' => 'base.review'])

@endsection
