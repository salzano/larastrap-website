@extends('layout.docs', [
    'title' => 'Tabs | Larastrap',
    'claim' => 'How to use the Boostrap5 Tabs component in Laravel',
])

@section('docs')

<h1>Tabs</h1>

<p class="mt-4">
    Tabs are composed by two essential elements: <x-larastrap::element>x-larastrap::tabs</x-larastrap::element> is the container wrapping all panels involved in the same block and providing actual clickable tabs, and <x-larastrap::element>x-larastrap::tabpane</x-larastrap::element> is a single activatable tab.
</p>

@include('partials.example', ['snippet' => 'tabs.base'])

<x-larastrap::title label="active" />

<p>
    You eventually want to specify an initially active tab (or no tab at all will be displayed by default). You can do this with the <x-larastrap::parameter>active</x-larastrap::parameter> parameter, applied to <x-larastrap::element>x-larastrap::tabs</x-larastrap::element> (must contain the index of the preferred tab) or to one of the contained <x-larastrap::element>x-larastrap::tabpane</x-larastrap::element> (boolean value).
</p>

@include('partials.example', ['snippet' => 'tabs.active'])

<x-larastrap::title label="tabview" />

@include('partials.example', ['snippet' => 'tabs.tabview'])

<x-larastrap::title label="use_anchors" />

<p>
    By default, clickable tabs are rendered as HTML button. But you can choose to render them as HTML anchors, with the <x-larastrap::parameter>use_anchors</x-larastrap::parameter> parameter.
</p>

@include('partials.example', ['snippet' => 'tabs.use_anchors'])

<p class="mt-4">
    Notably, the <x-larastrap::parameter>use_anchors</x-larastrap::parameter> parameter is automatically set to <x-larastrap::value>true</x-larastrap::value> when tabs are within a <a href="{{ route('docs.forms') }}">Form</a>. This is to avoid unintended behaviors, such as submitting the parent form when clicking on a child tab.
</p>

@include('partials.example', ['snippet' => 'tabs.form'])

<x-larastrap::title label="button_classes" />

<p>
    It is possible to customize the CSS classes applied to each tab, handling the <x-larastrap::parameter>button_classes</x-larastrap::parameter> parameter of each individual <x-larastrap::element>x-larastrap::tabpane</x-larastrap::element>.
</p>

@include('partials.example', ['snippet' => 'tabs.button_classes'])

<x-larastrap::title label="button_attributes" />

<p>
    In the same way, it is possible to customize the HTML attributes of each tab, handling the <x-larastrap::parameter>button_attributes</x-larastrap::parameter> parameter of each individual <x-larastrap::element>x-larastrap::tabpane</x-larastrap::element>. This must be an associative array, with names and values of each attribute as keys/values.
</p>

@include('partials.example', ['snippet' => 'tabs.button_attributes'])

@endsection
