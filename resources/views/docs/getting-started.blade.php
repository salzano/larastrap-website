@extends('layout.docs', [
    'title' => 'Getting started with Larastrap',
    'claim' => 'Install ready-to-use Bootstrap5 components for your Laravel application',
])

@section('docs')

<h1>Getting Started</h1>

<x-larastrap::title label="Install" />

<p>
    Larastrap is packaged in <a href="https://packagist.org/">Packagist</a>, the main PHP repository. You can install it within your Laravel application using <a href="https://getcomposer.org/">Composer</a>.
</p>
<p>
    <x-larastrap::command value="composer install madbob/larastrap" />
</p>

<p>
    Then you probably want a configuration file, editable as you like, with a few default config and examples. To generate the <code>config/larastrap.php</code> file, run
</p>
<p>
    <x-larastrap::command value="php artisan vendor:publish --tag=config" />
</p>

<p>
    Be aware you still have to install Bootstrap assets (both CSS and JS) in your page, with you preferred method (<code>npm i bootstrap</code>, deeplinking the CDN or whatever). It is suggested to use the <a href="https://packagist.org/packages/laravel/ui">laravel/ui</a> package to start up your new application.
</p>
<p>
    <x-larastrap::command value="composer require laravel/ui" />
</p>

<x-larastrap::title label="Usage" />

<p>
    Larastrap is a collection of <a href="https://laravel.com/docs/blade#components">Blade Components</a>, as defined by the Laravel's documentation.
</p>

<p>
    Those can be used in any Blade template using the syntax
</p>
<pre><code class="language-php">&lt;x-larastrap::componentname /></code></pre>
<p>
    or, for <a href="{{ route('docs.containers') }}">Containers</a>
</p>
<pre><code class="language-php">&lt;x-larastrap::componentname>
    contents
&lt;/x-larastrap::componentname></code></pre>

<p>
    Each element accepts a variety of parameters, that can be passed inline using HTML attributes (whose name have to be prefixed with a <code>:</code> character when handling a PHP expression).
</p>
<pre><code class="language-php">&lt;x-larastrap::componentname id="a_static_string" :name="$a_php_variable" /></code></pre>

<p>
    Parameters can also be massively passed through the <x-larastrap::parameter>params</x-larastrap::parameter> parameter, in a single array.
</p>
<pre><code class="language-php">&lt;x-larastrap::componentname :params="['id' => a_static_string, 'name' => $a_php_variable]" /></code></pre>


<p>
    Remember that, as per native Blade Components' behavior, is also possible to dynamically invoke a component by his own name using the <x-larastrap::code>x-dynamic-component</x-larastrap::code> tag.
</p>
<pre><code class="language-php">&lt;x-dynamic-component :component="sprintf('larastrap::%s', $a_component_type)" :params="['id' => 'a_static_string', 'name' => $a_php_variable]" /></code></pre>

<p>
    Check the documentation of each different element to discover more about his usage, the accepted parameters, the implicit features and behaviors and more!
</p>

<x-larastrap::title label="Configure" />

<p>
    The whole configuration of Larastrap is centered on the many possible parameters assignable to each element, each having his own meaning.
</p>

<p>
    Parameters' values are evaluated accordingly to a given priority sequence:
</p>
<ul>
    <li>parameters defined inline within the element have the highest priority</li>
    <li>some parameter can be inherited by the parent <a href="{{ route('docs.containers') }}">Container</a></li>
    <li>then it is the turn for values got from the <code>config/larastrap.php</code> file
        <ul>
            <li><a href="{{ route('docs.custom-elements') }}">custom elements</a> may have their own defaults, in the <i>custom</i> array</li>
            <li>each base element may have his own defaults, within the <i>elements</i> array</li>
            <li>basic default are to be written in the <i>commons</i> array</li>
        </ul>
    </li>
    <li>finally, if no other value has been found, the defaults hard coded in the code are applied</li>
</ul>

<p>
    This hierarchy permits a fine-grained configuration: a global one, intended to keep the whole behaviors coherent within the application, and the ability to specify each parameter only when required.
</p>

<p>
    Given the following sample file:
</p>

<pre><code class="language-php">&lt;?php

return [
    'commons' => [
        'color' => 'danger',
    ],

    'elements' => [
        'button' => [
            'color' => 'success',
        ]
    ],

    'customs' => [
        'mybutton' => [
            'extends' => 'button',
            'params' => [
                'color' => 'warning',
            ]
        ]
    ],
];</code></pre>

<p>
    All elements will have a default <x-larastrap::parameter>color</x-larastrap::parameter> = <x-larastrap::value>danger</x-larastrap::value>, apart for <x-larastrap::element>x-larastrap::button</x-larastrap::element> that will have <x-larastrap::parameter>color</x-larastrap::parameter> = <x-larastrap::value>success</x-larastrap::value>. When instead using the custom element <x-larastrap::element>x-larastrap::mybutton</x-larastrap::element>, even if it extends the base <x-larastrap::element>x-larastrap::button</x-larastrap::element> type, it will have <x-larastrap::parameter>color</x-larastrap::parameter> = <x-larastrap::value>warning</x-larastrap::value>.
</p>

<x-larastrap::title label="The Stack" />

<p>
    Most of the internal state of Larastrap is kept into the <x-larastrap::code>LarastrapStack</x-larastrap::code> service container, a singleton instantiated by Larastrap's Service Provider. It exposes a few utility function, including one to override the configuration usually get from the <code>config/larastrap.php</code> file.
</p>

<pre><code class="language-php">&lt;?php

$stack = app()->make('LarastrapStack');

/*
    From here, all forms have use the horizontal formview (despite what is
    written in the configuration file).
    The configuration of all other component types is left untouched.
*/
$stack->overrideNodesConfig([
    'form' => [
        'formview' => 'horizontal'
    ]
]);

/*
    Restores the configuration from the file
*/
$stack->overrideNodesConfig(null);</code></pre>

@endsection
