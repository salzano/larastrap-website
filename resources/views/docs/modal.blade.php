@extends('layout.docs', [
    'title' => 'Modals | Larastrap',
    'claim' => 'How to use the Boostrap5 Modals component in Laravel',
])

@section('docs')

<h1>Modals</h1>

<x-larastrap::title label="Trigger" />

<p>
    First of all, you need some kind of trigger to open a modal. It may be a <a href="{{ route('docs.button') }}"><x-larastrap::element>x-larastrap::button</x-larastrap::element></a> with the <x-larastrap::parameter>triggers_modal</x-larastrap::parameter> parameter enabled.
</p>

@include('partials.example', ['snippet' => 'modals.base'])

<x-larastrap::title label="title" />

<p>
    A modal can have a <x-larastrap::parameter>title</x-larastrap::parameter>, to be displayed in the header (near the "close" button).
</p>

@include('partials.example', ['snippet' => 'modals.title'])

<x-larastrap::title label="buttons" />

<p>
    Modals can have one or many buttons in his footer. The default configuration includes just the button to dismiss it, but you can modify it (both in the global configuration or with an inline parameter). The value of the <x-larastrap::parameter>buttons</x-larastrap::parameter> parameter has to be an array of arrays, each rappresenting a button and with the parameters for the relevant <a href="{{ route('docs.button') }}">x-larastrap::button</a>.
</p>

@include('partials.example', ['snippet' => 'modals.buttons'])

<p class="mt-4">
    Also <x-larastrap::element>x-larastrap::form</x-larastrap::element> has <x-larastrap::parameter>buttons</x-larastrap::parameter>, and when a form appears into a modal the two arrays are merged and the form's buttons are placed in the modal's footer.
</p>

@include('partials.example', ['snippet' => 'modals.form'])

<x-larastrap::title label="size" />

<p>
    It is possible to specify a <x-larastrap::parameter>size</x-larastrap::parameter> for a modal, both as a short identifier (<x-larastrap::value>sm</x-larastrap::value>, <x-larastrap::value>lg</x-larastrap::value>, <x-larastrap::value>xl</x-larastrap::value>, or <x-larastrap::value>none</x-larastrap::value> for the default size) or as an array of classes to be applied for responsive behavior.
</p>

@include('partials.example', ['snippet' => 'modals.size'])

<x-larastrap::title label="scrollable" />

<p>
    Use the <x-larastrap::parameter>scrollable</x-larastrap::parameter> parameter to permit the modal's contents to scroll.
</p>

@include('partials.example', ['snippet' => 'modals.scrollable'])

@endsection
