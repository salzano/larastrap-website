@extends('layout.docs', [
    'title' => 'Fields | Larastrap',
    'claim' => 'How to use the Boostrap5 Fields component in Laravel',
])

@section('docs')

<h1>Fields</h1>

<p>
    A <x-larastrap::element>x-larastrap::field</x-larastrap::element> usually wraps an input within a <x-larastrap::element>x-larastrap::form</x-larastrap::element>, and is responsible for the layout of the input itself and his label. Each input field provides to init his own wrapping <x-larastrap::element>x-larastrap::field</x-larastrap::element> (until the <x-larastrap::parameter>squeeze</x-larastrap::parameter> parameter is not set to <x-larastrap::value>true</x-larastrap::value>), and there is no need to manually place it, but it is possible to create your own fields to wrap custom elements.
</p>

<x-larastrap::title label="label" />

<p>
    The base parameter for a field is his <x-larastrap::parameter>label</x-larastrap::parameter>, used as a description for the child content (which usually is an input box, but can be anything else).
</p>

@include('partials.example', ['snippet' => 'fields.base'])

<x-larastrap::title label="label_class" />

<p>
    The label can be customized assigning one or more CSS classes, using the <x-larastrap::parameter>label_class</x-larastrap::parameter> parameter.
</p>

@include('partials.example', ['snippet' => 'fields.labelclass'])

<x-larastrap::title label="label_html" />

<p>
    If you want to use a completely custom HTML label, you have to use the <x-larastrap::parameter>label_html</x-larastrap::parameter> parameter instead of <x-larastrap::parameter>label</x-larastrap::parameter>. Entities in plain <x-larastrap::parameter>label</x-larastrap::parameter> are escaped; when using <x-larastrap::parameter>label_html</x-larastrap::parameter>, those are not.
</p>

@include('partials.example', ['snippet' => 'fields.labelhtml'])

<x-larastrap::title label="label_width / input_width" />

<p>
    With the <x-larastrap::parameter>label_width</x-larastrap::parameter> and <x-larastrap::parameter>input_width</x-larastrap::parameter> parameters it is possible to define the width of each column, within the twelve columns system of Bootstrap. You can specify both an absolute value, or an array of breakpoints to obtain a responsive layout.
</p>

@include('partials.example', ['snippet' => 'fields.widths'])

<p class="mt-4">
    Those parameter are inherited from the parent <a href="{{ route('docs.containers') }}">Container</a>, to avoid to specify for each field. Even better: it is recommended to define those value in the <a href="{{ route('docs.getting-started', '#configure') }}">Larastrap's global configuration</a>, once and for all the application, and override them only when required for specific and occasional edge cases.
</p>

@include('partials.example', ['snippet' => 'fields.formwidths'])

<x-larastrap::title label="help" />

<p>
    The <x-larastrap::parameter>help</x-larastrap::parameter> parameter implements a classic Bootstrap's pattern: displays a muted text below the actual content, to be used as an explicit hint for the user.
</p>

@include('partials.example', ['snippet' => 'fields.help'])

<x-larastrap::title label="pophelp" />

<p>
    The <x-larastrap::parameter>pophelp</x-larastrap::parameter> parameter is an alternative to <x-larastrap::parameter>help</x-larastrap::parameter>, and provides a popover to be used for more detailed (or less invasive) descriptions of the purpose of each field.
</p>
<p>
    Remember you have to init the Javascript function to handle Bootstrap's popovers!
</p>

@include('partials.example', ['snippet' => 'fields.pophelp'])

<x-larastrap::title label="squeeze" />

<p>
    It is possible to cut the wrapping field using the <x-larastrap::parameter>squeeze</x-larastrap::parameter> parameter.
</p>

@include('partials.example', ['snippet' => 'fields.squeeze'])

<p class="mt-4">
    This is mostly intended for usage within an actual input, and setup it to omit the field when a label is not required (e.g. within a table).
</p>

@include('partials.example', ['snippet' => 'fields.table'])

@endsection
