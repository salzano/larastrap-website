@extends('layout.docs', [
    'title' => 'Containers | Larastrap',
    'claim' => 'How to use the Boostrap5 Containers component in Laravel',
])

@section('docs')

<h1>Containers</h1>

<p class="mt-4">
    Larastrap provides many "container" components: those having a child HTML block to be wrapped within the container itself. All of them share a few parameters.
</p>

<x-larastrap::title label="obj" />

<p>
    <x-larastrap::parameter>obj</x-larastrap::parameter> is used in particular for <x-larastrap::element>x-larastrap::form</x-larastrap::element>, but can be used in any Container to define a source for values in child nodes.
</p>

@include('partials.example', ['snippet' => 'containers.obj'])

@endsection
