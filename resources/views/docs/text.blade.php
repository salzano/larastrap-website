@extends('layout.docs', [
    'title' => 'Typography | Larastrap',
    'claim' => 'How to use Boostrap5 Typography in Laravel',
])

@section('docs')

<h1>Typography</h1>

<p class="mt-4">
    Larastrap provides a generic utility component called <x-larastrap::element>x-larastrap::t</x-larastrap::element>, able to be rendered as any preferred HTML tag type.
</p>

@include('partials.example', ['snippet' => 'typography.base'])

<p class="mt-4">
    Of course this is not meant to be directly used in templates - as it would be tedious and unconvenient to write so much code just to open and close a <x-larastrap::code>span</x-larastrap::code> - but is considered a base to define your own <a href="{{ route('docs.custom-elements') }}">Custom Elements</a>, so to define the typography style of your whole application and keep it consistent through the centralized configuration.
</p>

@include('partials.custom', ['snippet' => 'customs.typography'])

<x-larastrap::title label="obj / name" />

<p>
    <x-larastrap::element>x-larastrap::t</x-larastrap::element> is a <a href="{{ route('docs.containers') }}">Container</a>, and an <x-larastrap::parameter>obj</x-larastrap::parameter> can be assigned (or inherited), but it also sports a few other parameters that permits you to display the value from a named attribute of the object (eventually: an <a href="https://laravel.com/docs/eloquent">Eloquent model</a>) directly within the textual node
</p>
<ul>
    <li><x-larastrap::parameter>name</x-larastrap::parameter> - same as <a href="{{ route('docs.input') }}">in Inputs</a>, define the name of the attribute to access</li>
    <li><x-larastrap::parameter>prelabel / postlabel</x-larastrap::parameter> - same as <a href="{{ route('docs.button', '#prelabel-postlabel') }}">in Buttons</a>, permit to prepend or append an arbitrary text</li>
</ul>

@include('partials.example', ['snippet' => 'typography.obj'])

<p class="mt-4">
    In this case, also, it is possible to define your own <a href="{{ route('docs.custom-elements') }}">Custom Elements</a> to always display the same attribute with the same style, decorations, addictional graphics and more.
</p>

@include('partials.custom', ['snippet' => 'customs.card'])

@endsection
