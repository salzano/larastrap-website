@extends('layout.docs', [
    'title' => 'Button Groups | Larastrap',
    'claim' => 'How to use the Boostrap5 Button Groups component in Laravel',
])

@section('docs')

<h1>Button Groups</h1>

<p class="mt-4">
    <x-larastrap::element>x-larastrap::btngroup</x-larastrap::element> is just a container for buttons, intended to keep a compact layout.
</p>

@include('partials.example', ['snippet' => 'button-groups.base'])

<x-larastrap::title label="orientation" />

<p>
    The only relevant optional parameter is <x-larastrap::parameter>orientation</x-larastrap::parameter>, which permits to have an <x-larastrap::value>horizontal</x-larastrap::value> (by default) or <x-larastrap::value>vertical</x-larastrap::value> layout.
</p>

@include('partials.example', ['snippet' => 'button-groups.vertical'])

@endsection
