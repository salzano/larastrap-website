@extends('layout.docs', [
    'title' => 'Forms | Larastrap',
    'claim' => 'How to use the Boostrap5 Forms component in Laravel',
])

@section('docs')

<h1>Forms</h1>

<p class="mt-4">
    <x-larastrap::element>x-larastrap::form</x-larastrap::element> is one of the most complex (and useful) components in Larastrap. It plays well with <a href="{{ route('docs.field') }}"><x-larastrap::element>x-larastrap::field</x-larastrap::element></a> to provide a powerful tool to organize, populate and handle the forms of your Laravel application.
</p>

<x-larastrap::title label="obj" />

<p>
    As all other <a href="{{ route('docs.containers') }}">Containers</a>, forms have an <x-larastrap::parameter>obj</x-larastrap::parameter> parameter which can be populated with an object (eventually: an <a href="https://laravel.com/docs/eloquent">Eloquent model</a>). This parameter is inherited by all child input fields, which values are then inited with the proper attributes matched by their name.
</p>
<p>
    When <x-larastrap::parameter>obj</x-larastrap::parameter> is <x-larastrap::value>null</x-larastrap::value>, all child inputs are left with their explicit value (if set at all).
</p>

@include('partials.example', ['snippet' => 'forms.base'])

<x-larastrap::title label="action / method / baseaction" />

<p>
    <x-larastrap::parameter>action</x-larastrap::parameter> and <x-larastrap::parameter>method</x-larastrap::parameter> parameters are equivalent to the classic HTML attributes for forms. But you can specify once a default <x-larastrap::parameter>method</x-larastrap::parameter> in your configuration file (by default it is <x-larastrap::value>POST</x-larastrap::value>).
</p>
<p>
    A bit more articulated is <x-larastrap::parameter>baseaction</x-larastrap::parameter>: this is intended to be the prefix of a <a href="https://laravel.com/docs/controllers#resource-controllers">Laravel's Resource Controller</a>.
</p>
<p>
    In the following example, when <x-larastrap::parameter>obj</x-larastrap::parameter> is set to <x-larastrap::value>NULL</x-larastrap::value> it is assigned as <x-larastrap::parameter>action</x-larastrap::parameter> the value <code>route('user.store')</code>; otherwise, if <x-larastrap::parameter>obj</x-larastrap::parameter> is a valid object, it will become <code>route('user.update', $obj->id)</code>. This is handy to have a single edit form to both create and update the same kind of model.
</p>
<p>
    Of course, when in update mode, the proper <x-larastrap::value>PUT</x-larastrap::value> <x-larastrap::parameter>method</x-larastrap::parameter> is used.
</p>

@include('partials.example', ['snippet' => 'forms.baseaction'])

<x-larastrap::title label="buttons" />

<p>
    The form's buttons (notably: the submit button) are generally not included within the <x-larastrap::element>x-larastrap::form</x-larastrap::element> node but are defined using the <x-larastrap::parameter>buttons</x-larastrap::parameter> parameter, which has to be an array of arrays, each rappresenting a button and with the parameters for the relevant <a href="{{ route('docs.button') }}">x-larastrap::button</a>.
</p>

@include('partials.example', ['snippet' => 'forms.buttons'])

<x-larastrap::title label="buttons_align" />

<p>
    The <x-larastrap::parameter>buttons_align</x-larastrap::parameter> parameter permit to define where to put <x-larastrap::parameter>buttons</x-larastrap::parameter> within the form: by default their are aligned to <x-larastrap::value>end</x-larastrap::value>, but can also be at <x-larastrap::value>start</x-larastrap::value> or <x-larastrap::value>center</x-larastrap::value>.
</p>

@include('partials.example', ['snippet' => 'forms.buttonsalign'])

<x-larastrap::title label="formview" />

<p>
    It is possible to organize the layout of the Form in multiple ways, using the <x-larastrap::parameter>formview</x-larastrap::parameter> parameter. <x-larastrap::value>horizontal</x-larastrap::value> is the default value and behavior, but it is possible to opt for <x-larastrap::value>inline</x-larastrap::value>; it this case, please note that labels are converted in placeholders to accomodate into the row.
</p>

@include('partials.example', ['snippet' => 'forms.inline'])

<p class="mt-4">
    Another option is the <x-larastrap::value>vertical</x-larastrap::value> value for <x-larastrap::parameter>formview</x-larastrap::parameter>.
</p>

@include('partials.example', ['snippet' => 'forms.vertical'])

<x-larastrap::title label="Errors Handling" />

<p>
    Larastrap's forms have built-in integration with <a href="https://laravel.com/docs/validation">Laravel's Validation</a>, and a few parameters to deal with it.
</p>
<p>
    By default <x-larastrap::parameter>error_handling</x-larastrap::parameter> is set to <x-larastrap::value>true</x-larastrap::value> and <x-larastrap::parameter>error_bag</x-larastrap::parameter> is set to <x-larastrap::value>default</x-larastrap::value>. To ignore validation just set <x-larastrap::parameter>error_handling</x-larastrap::parameter> to <x-larastrap::value>false</x-larastrap::value>. To change <a href="https://laravel.com/docs/validation#named-error-bags">the name of the "error bag"</a> used by your server-side Laravel's validator, set <x-larastrap::parameter>error_bag</x-larastrap::parameter> accordingly.
</p>

@include('partials.example', ['snippet' => 'forms.validation'])

<p class="mt-4">
    To, indeed, leverage Bootstrap's client-side validation functionality, use the <x-larastrap::parameter>client_side_errors</x-larastrap::parameter> parameter to deploy the relevant attributes in the HTML markup (and do not forget to init the proper Javascript code).
</p>

@include('partials.example', ['snippet' => 'forms.validationnative'])

@endsection
