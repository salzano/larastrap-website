@extends('layout.docs', [
    'title' => 'Buttons | Larastrap',
    'claim' => 'How to use the Boostrap5 Buttons component in Laravel',
])

@section('docs')

<h1>Buttons</h1>

<p class="mt-4">
    Buttons can be used as standalone elements or - more frequently - combined with <a href="{{ route('docs.forms') }}">Forms</a> and <a href="{{ route('docs.modal') }}">Modals</a>. In the latter case, you can specify the proper parameters within the dedicated parent's attribute (usually: <x-larastrap::parameter>buttons</x-larastrap::parameter>) instead to place the <x-larastrap::element>x-larastrap::button</x-larastrap::element> node in your template.
</p>

<x-larastrap::title label="label / color" />

<p>
    A <x-larastrap::element>x-larastrap::button</x-larastrap::element> is defined essentially by a <x-larastrap::parameter>label</x-larastrap::parameter> and, eventually, a <x-larastrap::parameter>color</x-larastrap::parameter> (one defined by Bootstrap; defaults to <x-larastrap::value>primary</x-larastrap::value>).
</p>

@include('partials.example', ['snippet' => 'buttons.base'])

<x-larastrap::title label="size" />

<p>
    Bootstrap provides three different sizes for buttons: the default one, <x-larastrap::value>sm</x-larastrap::value> and <x-larastrap::value>lg</x-larastrap::value>. Those can be set using the <x-larastrap::parameter>size</x-larastrap::parameter> parameter.
</p>

@include('partials.example', ['snippet' => 'buttons.size'])

<x-larastrap::title label="prelabel / postlabel" />

<p>
    A peculiar feature is the ability to define a prefix or a postfix to the <x-larastrap::parameter>label</x-larastrap::parameter>, using <x-larastrap::parameter>prelabel</x-larastrap::parameter> or <x-larastrap::parameter>postlabel</x-larastrap::parameter>.
</p>

@include('partials.example', ['snippet' => 'buttons.labels'])

<p class="mt-4">
    The primary usage of this feature is to define your own <a href="{{ route('docs.custom-elements') }}">custom elements</a> placing texts, icons and other distinctive traits to buttons having the same purpose within the application.
</p>

@include('partials.custom', ['snippet' => 'buttons.custom-modal'])

<x-larastrap::title label="triggers_collapse / triggers_modal" />

<p>
    As buttons are usually used to trigger <a href="{{ route('docs.modal') }}">Modals</a> and <a href="{{ route('docs.collapse') }}">Collapses</a>, the relative <x-larastrap::parameter>triggers_modal</x-larastrap::parameter> and <x-larastrap::parameter>triggers_collapse</x-larastrap::parameter> parameters are conveniently provided: those will generate the HTML attributes used by Bootstrap to execute the proper JS functions.
</p>
<p>
    Both must contain the HTML ID of the target node, with or without the prefix <x-larastrap::code>#</x-larastrap::code> to complete the CSS selector (if missing, it is automatically added).
</p>

@include('partials.example', ['snippet' => 'buttons.triggers'])

<x-larastrap::title label="Alternative: Links" />

<p>
    An alternative to <x-larastrap::element>x-larastrap::button</x-larastrap::element> is <x-larastrap::element>x-larastrap::link</x-larastrap::element>, which generates an actual link (an <x-larastrap::code>&lt;a></x-larastrap::code> HTML node). This component has all of the button's parameters, plus <x-larastrap::parameter>href</x-larastrap::parameter>.
</p>

@include('partials.example', ['snippet' => 'buttons.link'])

@endsection
