@extends('layout.docs', [
    'title' => 'Enclose | Larastrap',
    'claim' => 'How to use the Boostrap5 Enclose component in Laravel',
])

@section('docs')

<h1>Enclose</h1>

<p class="mt-4">
    "Enclose" is a peculiar type of element provided by Larastrap: similar to a <a href="{{ route('docs.forms') }}">Form</a>, it acts as a logical container for other elements without providing any actual HTML markup.
</p>

@include('partials.example', ['snippet' => 'encloses.base'])

<p class="mt-4">
    His may purpose is to isolate different models within the same logical block. The common use case is: a form with a table involving multiple models at once, each with his own attributes (and his own input fields).
</p>

@include('partials.example', ['snippet' => 'encloses.table'])

<p class="mt-4">
    In the example above, it is important to note the use of <a href="{{ route('docs.input', '#nprefix-npostfix') }}"><x-larastrap::parameter>npostfix</x-larastrap::parameter></a> parameter: when the form is submitted, all email addresses have to be serialized into an array and sent to the specified endpoint (the form's <x-larastrap::parameter>action</x-larastrap::parameter>).
</p>

@endsection
