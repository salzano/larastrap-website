@extends('layout.docs', [
    'title' => 'Selects | Larastrap',
    'claim' => 'How to use the Boostrap5 Select component in Laravel',
])

@section('docs')

<h1>Selects</h1>

<x-larastrap::title label="options" />

<p>
    <x-larastrap::element>x-larastrap::select</x-larastrap::element> is the equivalent of a "select" HTML node, with his own <x-larastrap::parameter>options</x-larastrap::parameter>.
</p>

@include('partials.example', ['snippet' => 'selects.base'])

<p class="mt-4">
    <x-larastrap::parameter>options</x-larastrap::parameter> can be a simple associative array where keys are the HTML values and values are the related labels. But each value in the options can also be an object, holding a few attributes to better describe each item. Among them:
</p>
<ul>
    <li><x-larastrap::code>label</x-larastrap::code> - the label to display</li>
    <li><x-larastrap::code>disabled</x-larastrap::code> - set true if the option is not selectable</li>
    <li><x-larastrap::code>chldren</x-larastrap::code> - an equivalent array of options, to be used to render optgroups</li>
</ul>

@include('partials.example', ['snippet' => 'selects.options'])

<x-larastrap::title label="With Models" />

<p>
    The <x-larastrap::element>x-larastrap::select-model</x-larastrap::element> variant permits to use objects (eventually: <a href="https://laravel.com/docs/eloquent">Eloquent models</a>) as options.
</p>

@include('partials.example', ['snippet' => 'selects.models'])

<p class="mt-4">
    The <x-larastrap::code>-model</x-larastrap::code> variants are able to set their own value accordingly to the <a href="https://laravel.com/docs/eloquent-relationships">Eloquent relationships</a> of the reference entity (the one assigned as <x-larastrap::parameter>obj</x-larastrap::parameter> of the parent <x-larastrap::element>x-larastrap::form</x-larastrap::element>), as long as their <x-larastrap::parameter>name</x-larastrap::parameter> matches the attribute to which the relationship itself is exposed.
</p>

@include('partials.example', ['snippet' => 'selects.modelsselection'])

<p class="mt-4">
    For <x-larastrap::code>-model</x-larastrap::code> variant it is assumed that models into the <x-larastrap::parameter>options</x-larastrap::parameter> collection have an "id" and a "name" attribute to be used (respectively: as the value and the label of each item).
</p>
<p>
    If your models have a different structure, or you want to display different labels, it is possible to pass a <x-larastrap::parameter>translateCallback</x-larastrap::parameter> parameter with a function to translate your actual content: it has to accept a parameter (the model to be translated) and must return an array with two elements (the value and the label of the final selectable option).
</p>

@include('partials.example', ['snippet' => 'selects.modelscallback'])

<p class="mt-4">
    The <x-larastrap::code>-model</x-larastrap::code> variants have also an <x-larastrap::parameter>extra_options</x-larastrap::parameter> parameter to define other options aside those collected in <x-larastrap::parameter>options</x-larastrap::parameter>. This is useful to add a "void" item to be choosen by the user.
</p>

@include('partials.example', ['snippet' => 'selects.modelsextra'])

@endsection
