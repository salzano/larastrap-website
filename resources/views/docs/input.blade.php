@extends('layout.docs', [
    'title' => 'Inputs | Larastrap',
    'claim' => 'How to use the Boostrap5 Input components in Laravel',
])

@section('docs')

<h1>Inputs</h1>

<p>
    Larastrap provides multiple components implementing the many HTML input types. Some of them have specific parameters.
</p>

@include('partials.example', ['snippet' => 'inputs.base'])

<x-larastrap::title label="name" />

<p>
    The <x-larastrap::parameter>name</x-larastrap::parameter> parameter has two purposes: assign the relative HTML attribute to the input node, and access the same-named attribute of the undelying <x-larastrap::parameter>obj</x-larastrap::parameter> assigned to the parent <x-larastrap::element>x-larastrap::form</x-larastrap::element>.
</p>

@include('partials.example', ['snippet' => 'inputs.name'])

<x-larastrap::title label="nprefix / npostfix" />

<p>
    With <x-larastrap::parameter>nprefix</x-larastrap::parameter> and <x-larastrap::parameter>npostfix</x-larastrap::parameter> it is possible to prepend or append a string to the actual HTML "name" attribute of the node. This permits to decouple from the name of the <x-larastrap::parameter>obj</x-larastrap::parameter> attribute you want to use to populate the input field.
</p>
<p>
    Useful in particular to handle arrays of objects within a form.
</p>

@include('partials.example', ['snippet' => 'inputs.nfix'])

<x-larastrap::title label="textappend" />

<p>
    When set with a value, the <x-larastrap::parameter>textappend</x-larastrap::parameter> parameter enforces the creation of a Bootstrap's "input group" and the addiction of an explicit label after the input field itself.
</p>

@include('partials.example', ['snippet' => 'inputs.textappend'])

<x-larastrap::title label="number and range" />

<p>
    <x-larastrap::element>x-larastrap::number</x-larastrap::element> and <x-larastrap::element>x-larastrap::range</x-larastrap::element> have - like the equivalent HTML input elements - a few parameters that permit to alter the behavior (and the validation) of them.
</p>

@include('partials.example', ['snippet' => 'inputs.number'])

@endsection
