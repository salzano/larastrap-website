@extends('layout.docs', [
    'title' => 'Accordions | Larastrap',
    'claim' => 'How to use the Boostrap5 Accordion component in Laravel',
])

@section('docs')

<h1>Accordions</h1>

<p class="mt-4">
    Accordions are composed by two essential elements: <x-larastrap::element>x-larastrap::accordion</x-larastrap::element> is the container wrapping all panels involved in the same block, and <x-larastrap::element>x-larastrap::accordionitem</x-larastrap::element> is a single collapsible panel within the block.
</p>

@include('partials.example', ['snippet' => 'accordions.base'])

<x-larastrap::title label="active" />

<p>
    With the <x-larastrap::parameter>active</x-larastrap::parameter> parameter applied to <x-larastrap::element>x-larastrap::accordionitem</x-larastrap::element> it is possible to specify which panels display as opened by default.
</p>

@include('partials.example', ['snippet' => 'accordions.active'])

<x-larastrap::title label="alway_open" />

<p>
    When the parameter <x-larastrap::parameter>alway_open</x-larastrap::parameter> is set to <x-larastrap::value>false</x-larastrap::value> on a <x-larastrap::element>x-larastrap::accordion</x-larastrap::element> (the default), opening a panel implies close the others. When set to <x-larastrap::value>true</x-larastrap::value>, the uncollapsed panel stay opened even when another one is triggered.
</p>

@include('partials.example', ['snippet' => 'accordions.always_open'])

@endsection
