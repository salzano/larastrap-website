<x-larastrap::navbar :options="[
    'First' => ['route' => 'homepage'],
    'Second' => ['route' => 'docs.navbar'],
    'More' => ['children' => [
        'Forms' => ['route' => 'docs.forms'],
        'Modals' => ['route' => 'docs.modal'],
    ]]
]" />
