<x-larastrap::navbar :options="[
    'Home' => ['route' => 'homepage'],
    'Getting Started' => ['route' => 'docs.getting-started'],
]" :end_options="[
    'Modals' => route('docs.modal'),
    'Tabs' => route('docs.tabs'),
]" />
