<x-larastrap::navbar color="light" :options="[
    'Home' => '/',
    'Getting Started' => '/docs/getting-started',
    'Base Element' => '/docs/components/element',
]" />

<br>

<x-larastrap::navbar color="dark" :options="[
    'Home' => '/',
    'Getting Started' => '/docs/getting-started',
    'Base Element' => '/docs/components/element',
]" />
