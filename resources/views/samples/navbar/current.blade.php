<x-larastrap::navbar :options="[
    'Home' => '/',
    'Navbar' => '/docs/components/navbar',
    'Base Element' => '/docs/components/element',
]" />

<br>

<x-larastrap::navbar :options="[
    'Home' => ['route' => 'homepage'],
    'Navbar' => ['route' => 'docs.navbar'],
    'Base Element' => ['route' => 'docs.element'],
]" />
