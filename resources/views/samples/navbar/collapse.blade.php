<x-larastrap::navbar collapse="never" :options="[
    'Home' => '/',
    'Navbar' => '/docs/components/navbar',
    'Base Element' => '/docs/components/element',
]" />

<br>

<x-larastrap::navbar collapse="md" :options="[
    'Home' => '/',
    'Navbar' => '/docs/components/navbar',
    'Base Element' => '/docs/components/element',
]" />

<br>

<x-larastrap::navbar collapse="lg" :options="[
    'Home' => '/',
    'Navbar' => '/docs/components/navbar',
    'Base Element' => '/docs/components/element',
]" />

<br>

<x-larastrap::navbar collapse="xl" :options="[
    'Home' => '/',
    'Navbar' => '/docs/components/navbar',
    'Base Element' => '/docs/components/element',
]" />

<br>

<x-larastrap::navbar collapse="xxl" :options="[
    'Home' => '/',
    'Navbar' => '/docs/components/navbar',
    'Base Element' => '/docs/components/element',
]" />
