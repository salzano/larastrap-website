@php

$options = [
    'first' => (object) [
        'label' => 'First',
        'id' => 'first',
    ],
    'second' => (object) [
        'label' => 'Second',
        'disabled' => true,
    ],
    'third' => (object) [
        'label' => 'Third',
        'button_attributes' => ['data-bs-toggle' => 'tooltip', 'data-bs-title' => 'Sample tooltip'],
    ],
    'fourth' => (object) [
        'label' => 'Fourth',
        'hidden' => true,
    ],
    'fifth' => (object) [
        'label' => 'Fifth',
        'button_classes' => ['text-white', 'btn-danger'],
    ],
];

@endphp

<x-larastrap::checks label="Choose Many" :options="$options" />
<x-larastrap::radios label="Choose One" :options="$options" />
