@php

// In this example, the User model has a "boss" relationship defined as
//
// public function boss()
// {
//     return $this->belongsTo(User::class);
// }
//
// and a "friends" relationship defined as
//
// public function friends()
// {
//     return $this->belongsToMany(User::class);
// }

$ref = App\Models\User::has('boss')->inRandomOrder()->first();
$bosses = App\Models\User::doesntHave('boss')->orderBy('name', 'asc')->get();
$users = App\Models\User::orderBy('name', 'asc')->get();

@endphp

<x-larastrap::form :obj="$ref">
    <x-larastrap::radios-model name="boss" :label="sprintf('Select a Boss for %s', $ref->name)" :options="$bosses" />
    <x-larastrap::checklist-model name="friends" :label="sprintf('Select friends of %s', $ref->name)" :options="$users" />
</x-larastrap::form>
