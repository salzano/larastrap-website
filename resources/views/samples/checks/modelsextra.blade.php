@php

$users = App\Models\User::inRandomOrder()->take(3)->get();

@endphp

<x-larastrap::radios-model label="Select a User" :options="$users" :extra_options="[0 => 'None']" value="0" />
