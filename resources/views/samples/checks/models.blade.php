@php

$users = App\Models\User::inRandomOrder()->take(3)->get();

@endphp

<x-larastrap::radios-model label="Select a User" :options="$users" />
<x-larastrap::checks-model label="Select Many Users" :options="$users" />
<x-larastrap::radiolist-model label="Select a User" :options="$users" />
<x-larastrap::checklist-model label="Select Many Users" :options="$users" />
