@php

$options = ['red' => 'Red', 'green' => 'Green', 'blue' => 'Blue'];

@endphp

<x-larastrap::radiolist label="Choose One" :options="$options" />
<x-larastrap::checklist label="Choose Many" :options="$options" />
