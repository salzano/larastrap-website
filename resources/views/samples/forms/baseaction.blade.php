<x-larastrap::form :obj="null" baseaction="user">
    <p>
        This form creates a new model.
    </p>

    <x-larastrap::text name="name" label="Name" />
    <x-larastrap::email name="email" label="EMail" />
</x-larastrap::form>

<br>

@php

$obj = (object) [
    'id' => 123,
    'name' => 'Foo Bar',
    'email' => 'foobar@example.com',
];

@endphp

<x-larastrap::form :obj="$obj" baseaction="user">
    <p>
        This form updates the existing model.
    </p>

    <x-larastrap::text name="name" label="Name" />
    <x-larastrap::email name="email" label="EMail" />
</x-larastrap::form>
