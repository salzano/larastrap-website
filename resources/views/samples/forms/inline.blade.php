<x-larastrap::form formview="inline">
    <x-larastrap::text name="firstname" label="First Name" />
    <x-larastrap::text name="lastname" label="Last Name" />
    <x-larastrap::email name="email" label="EMail" />
</x-larastrap::form>
