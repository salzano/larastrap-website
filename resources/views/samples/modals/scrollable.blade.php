<x-larastrap::button triggers_modal="test-scrollable" label="Open Modal" />

<x-larastrap::modal id="test-scrollable" scrollable="true">
    <p>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Sed elementum tempus egestas sed sed risus pretium. Felis donec et odio pellentesque diam volutpat commodo sed. Turpis massa sed elementum tempus egestas sed sed. Arcu dictum varius duis at consectetur. Viverra tellus in hac habitasse platea dictumst vestibulum rhoncus. Ut ornare lectus sit amet est placerat in egestas. Cum sociis natoque penatibus et magnis. Habitasse platea dictumst quisque sagittis purus sit amet volutpat. At ultrices mi tempus imperdiet nulla. Faucibus ornare suspendisse sed nisi lacus. Volutpat consequat mauris nunc congue nisi vitae suscipit tellus mauris. At varius vel pharetra vel turpis nunc eget lorem dolor. Vulputate mi sit amet mauris commodo quis.
    </p>

    <p>
        Amet facilisis magna etiam tempor orci eu. Sollicitudin tempor id eu nisl nunc mi ipsum faucibus vitae. Eu consequat ac felis donec et odio pellentesque. Sed augue lacus viverra vitae. Dui nunc mattis enim ut tellus elementum. Sem et tortor consequat id porta nibh venenatis cras. Ante metus dictum at tempor commodo. Et netus et malesuada fames ac turpis. Amet nisl purus in mollis nunc sed. Sit amet cursus sit amet dictum sit amet justo. Pretium vulputate sapien nec sagittis aliquam. Eget nullam non nisi est sit. Purus non enim praesent elementum facilisis leo vel fringilla. Purus in massa tempor nec.
    </p>

    <p>
        Rhoncus urna neque viverra justo nec ultrices. Est pellentesque elit ullamcorper dignissim cras tincidunt lobortis feugiat. Sit amet est placerat in egestas erat. Mauris in aliquam sem fringilla ut morbi tincidunt. Nisi quis eleifend quam adipiscing vitae. Tempor commodo ullamcorper a lacus vestibulum sed arcu. Rutrum quisque non tellus orci ac auctor augue mauris. Nunc vel risus commodo viverra maecenas accumsan lacus vel. Odio facilisis mauris sit amet massa. Nunc id cursus metus aliquam eleifend mi. Auctor elit sed vulputate mi sit amet mauris. Aenean et tortor at risus. Gravida rutrum quisque non tellus orci ac auctor. Turpis egestas pretium aenean pharetra magna ac placerat vestibulum lectus. Nunc sed augue lacus viverra vitae congue.
    </p>
</x-larastrap::modal>
