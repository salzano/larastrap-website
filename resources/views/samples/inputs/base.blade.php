<x-larastrap::form>
    <x-larastrap::hidden />
    <x-larastrap::text label="A Text" />
    <x-larastrap::textarea label="A Textarea" />
    <x-larastrap::number label="A Number" />
    <x-larastrap::email label="An Email" />
    <x-larastrap::url label="An URL" />
    <x-larastrap::range label="A Range" />
    <x-larastrap::date label="A Date" />
    <x-larastrap::time label="A Time" />
    <x-larastrap::datetime label="Both Date and Time" />
    <x-larastrap::color label="A Color" />
    <x-larastrap::check label="A Boolean" />
    <x-larastrap::radios label="Choose One" :options="['red' => 'Red', 'green' => 'Green', 'blue' => 'Blue']" />
    <x-larastrap::checks label="Choose Many" :options="['red' => 'Red', 'green' => 'Green', 'blue' => 'Blue']" />
</x-larastrap::form>
