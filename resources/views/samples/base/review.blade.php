<x-larastrap::button label="Test" :reviewCallback="function($button, $params) {
    $params['label'] = $params['label'] . ' ' . date('d/m/Y H:i:s');
    return $params;
}" />
