[
    'customs' => [
        'command' => [
            'extends' => 'text',
            'params' => [
                'classes' => ['command'],
                'attributes' => ['readonly' => 'readonly'],
                'textappend' => '<i class="bi bi-clipboard"></i>',
                'squeeze' => true
            ]
        ]
    ]
]
