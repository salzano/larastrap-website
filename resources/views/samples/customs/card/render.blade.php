@php
$users = App\Models\User::inRandomOrder()->take(3)->get();
@endphp

<div class="card-group">
    @foreach($users as $user)
        <x-larastrap::enclose :obj="$user">
            <div class="card">
                <div class="card-body">
                    <x-larastrap::ctitle />
                    <x-larastrap::cmail />
                </div>
            </div>
        </x-larastrap::enclose>
    @endforeach
</div>
