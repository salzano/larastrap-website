[
    'customs' => [
        'element' => [
            'extends' => 't',
            'params' => [
                'classes' => ['text-danger'],
                'node' => 'strong',
            ]
        ],

        'value' => [
            'extends' => 't',
            'params' => [
                'node' => 'code',
            ]
        ],

        'parameter' => [
            'extends' => 't',
            'params' => [
                'node' => 'strong',
            ]
        ]
    ]
]
