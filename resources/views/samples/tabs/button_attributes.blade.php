<x-larastrap::tabs active="0">
    <x-larastrap::tabpane label="First">
        <p>This is the first tab!</p>
    </x-larastrap::tabpane>

    <x-larastrap::tabpane label="Second" :button_attributes="['data-custom' => 'this-is-a-test']">
        <p>This is the second tab!</p>
    </x-larastrap::tabpane>
</x-larastrap::tabs>
