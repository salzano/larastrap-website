<x-larastrap::accordion>
    <x-larastrap::accordionitem label="First">
        <p>This is the first accordion and is closed by default</p>
    </x-larastrap::accordionitem>

    <x-larastrap::accordionitem label="Second" active="true">
        <p>This is the second accordion and is opened by default</p>
    </x-larastrap::accordionitem>

    <x-larastrap::accordionitem label="Third">
        <p>This is the third accordion and is closed by default</p>
    </x-larastrap::accordionitem>
</x-larastrap::accordion>
