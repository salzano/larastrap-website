<x-larastrap::accordion>
    <x-larastrap::accordionitem label="First">
        <p>This is the first accordion</p>
    </x-larastrap::accordionitem>

    <x-larastrap::accordionitem label="Second">
        <p>This is the second accordion</p>
    </x-larastrap::accordionitem>

    <x-larastrap::accordionitem label="Third">
        <p>This is the third accordion</p>
    </x-larastrap::accordionitem>
</x-larastrap::accordion>
