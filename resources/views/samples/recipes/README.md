# Submit your own Larastrap Recipe!

Larastrap Recipe is a collection of <a href="https://larastrap.madbob.org/#customs">Custom Elements</a> and tricky configurations for <a href="https://larastrap.madbob.org/">Larastrap</a>, to share reusable utilities and handful suggestions for the library usage.

All are invited to share their owns with a Pull Request in this repository!

Each folder here is a recipe, and contains:

* description.txt: an HTML content that describes the recipe
* config.php: the array of configurations that define your custom element
* render.blade.php: a Blade file to showcase the recipe

If the recipe requires more contents to be executed (a Model on the database, a specific route...), please describe them within your Pull Request.
