[
    'customs' => [
        'username' => [
            'extends' => 'text',
            'params' => [
                'attributes' => [
                    'autocomplete' => 'off',
                    'autocorrect' => 'off',
                    'autocapitalize' => 'none',
                    'spellcheck' => 'false',
                ]
            ],
        ],
    ]
]
