<x-larastrap::datepicker label="Pick a Date" :value="time()" />

<!-- Inclusion of Bootstrap Datepicker CSS and JS, and initialization -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>

<script>
$('input.date').datepicker({
    format: 'DD dd MM yyyy',
    autoclose: true,
    clearBtn: true,
});
</script>
