[
    'customs' => [
        'testablechecks' => [
            'extends' => 'checks',
            'params' => [
                'reviewCallback' => function($component, $params) {
                    /*
                        Choose your method to determine if you are within a Dusk test
                    */
                    if (env('DUSK_TESTING')) {
                        $options = $params['options'];
                        $new_options = [];

                        foreach($options as $value => $option) {
                            if (is_object($option)) {
                                if (!isset($option->button_attributes)) {
                                    $option->button_attributes = [];
                                }
                            }
                            else {
                                $option = (object) [
                                    'label' => $option,
                                    'button_attributes' => [],
                                ];
                            }

                            $option->button_attributes['dusk'] = sprintf('%s-%s', $params['name'], $value);
                            $new_options[$value] = $option;
                        }

                        $params['options'] = $new_options;
                    }

                    return $params;
                }
            ],
        ],
    ]
]
