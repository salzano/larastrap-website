@php

$options = ['red' => 'Red', 'green' => 'Green', 'blue' => 'Blue'];

@endphp

<x-larastrap::select label="Choose One" :options="$options" />
