@php

$options = [
    'first' => (object) [
        'label' => 'First',
    ],
    'second' => (object) [
        'label' => 'Second',
        'disabled' => true,
    ],
    'third' => (object) [
        'label' => 'Third',
        'children' => [
            'third_1' => (object) [
                'label' => 'Third / First',
            ],
            'third_2' => (object) [
                'label' => 'Third / Second',
                'disabled' => true,
            ],
            'third_3' => (object) [
                'label' => 'Third / Third',
            ],
        ],
    ],
];

@endphp

<x-larastrap::select label="Choose One" :options="$options" />
