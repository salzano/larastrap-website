@php

// In this example, the User model has a "boss" relationship defined as
//
// public function boss()
// {
//     return $this->belongsTo(User::class);
// }

$ref = App\Models\User::has('boss')->inRandomOrder()->first();
$bosses = App\Models\User::doesntHave('boss')->orderBy('name', 'asc')->get();

@endphp

<x-larastrap::form :obj="$ref">
    <x-larastrap::select-model name="boss" :label="sprintf('Select a Boss for %s', $ref->name)" :options="$bosses" />
</x-larastrap::form>
