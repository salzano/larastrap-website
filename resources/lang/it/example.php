<?php

return [
    'label' => 'Questa è una etichetta',
    'help' => 'Questo è un breve testo di aiuto',
    'pophelp' => 'Questo è un testo di aiuto lungo',

    'name' => 'Nome',
    'email' => 'Indirizzo E-Mail',
];
