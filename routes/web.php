<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\CommonController;

Route::get('/', [CommonController::class, 'homepage'])->name('homepage');

Route::get('/reference', function() {
    return view('reference');
});

Route::prefix('docs')->group(function () {
    Route::get('/getting-started', function () {
        return view('docs.getting-started');
    })->name('docs.getting-started');

    Route::prefix('components')->group(function () {
        Route::get('/element', function () {
            return view('docs.element');
        })->name('docs.element');

        Route::get('/text', function () {
            return view('docs.text');
        })->name('docs.text');

        Route::get('/navbar', function () {
            return view('docs.navbar');
        })->name('docs.navbar');

        Route::get('/button', function () {
            return view('docs.button');
        })->name('docs.button');
    });

    Route::prefix('forms')->group(function () {
        Route::get('/overview', function () {
            return view('docs.forms');
        })->name('docs.forms');

        Route::get('/field', function () {
            return view('docs.field');
        })->name('docs.field');

        Route::get('/input', function () {
            return view('docs.input');
        })->name('docs.input');

        Route::get('/select', function () {
            return view('docs.select');
        })->name('docs.select');

        Route::get('/check-radio', function () {
            return view('docs.check-radio');
        })->name('docs.check-radio');
    });

    Route::prefix('containers')->group(function () {
        Route::get('/overview', function () {
            return view('docs.containers');
        })->name('docs.containers');

        Route::get('/button-group', function () {
            return view('docs.button-group');
        })->name('docs.button-group');

        Route::get('/modal', function () {
            return view('docs.modal');
        })->name('docs.modal');

        Route::get('/accordion', function () {
            return view('docs.accordion');
        })->name('docs.accordion');

        Route::get('/collapse', function () {
            return view('docs.collapse');
        })->name('docs.collapse');

        Route::get('/tabs', function () {
            return view('docs.tabs');
        })->name('docs.tabs');

        Route::get('/enclose', function () {
            return view('docs.enclose');
        })->name('docs.enclose');
    });

    Route::prefix('custom-elements')->group(function () {
        Route::get('/overview', [CommonController::class, 'customIndex'])->name('docs.custom-elements');
        Route::get('/examples', [CommonController::class, 'customExamples'])->name('docs.custom-examples');
    });

    Route::prefix('more')->group(function () {
        Route::get('/translations', function () {
            return view('docs.translations');
        })->name('docs.translations');
    });
});

Route::get('/recipes', function() {
    return redirect()->route('docs.custom-examples');
});

/*
    Those are used in examples
*/
Route::post('/user', [CommonController::class, 'fakePost'])->name('user.store');
Route::put('/user/{id}', [CommonController::class, 'fakePost'])->name('user.update');
Route::post('/document/save', [CommonController::class, 'fakePost'])->name('document.store');
Route::post('/do/nothing', [CommonController::class, 'fakePost'])->name('nothing');
Route::post('/validated', [CommonController::class, 'validatedRoute'])->name('validated');
